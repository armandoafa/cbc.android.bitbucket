package com.freeway.cbc;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freeway.cbc.controller.AccountController;
import com.freeway.cbc.controller.MainController;
import com.freeway.cbc.util.ServerUtilities;
import com.freeway.cbc.util.Utils;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ApiVersionStrings;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;
import com.salesforce.androidsdk.ui.SalesforceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CustomerDashboardActivity extends SalesforceActivity {

    private RestClient client;
    private ArrayAdapter<String> listAdapter;


    private Button btnSyncContract;
    private Button btnTodos;
    private Button btnPendiente;
    private Button btnProgreso;
    private Button btnCompletos;
    private ImageView imgRefresh;
    private ImageView imgAdd;


    private ImageView imgLocation;
    private ImageView imgLista;
    private ImageView imgFiltro;


    private EditText et;

    private ListView listView;

    private TextView txtProgreso;

    private JSONObject oData;
    private JSONArray filterData;

    private MainController mainController;

    String accountType;
    ClientManager.LoginOptions loginOptions;
    ClientManager clientManager;
    RestClient restClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServerUtilities.context = getApplicationContext();
        MainController.activeIntentDashBoard(ServerUtilities.context);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_customer_dashboard);
        listView = (ListView) findViewById(R.id.lvCustomer);
        txtProgreso = (TextView) findViewById(R.id.textViewProgreso);

        mainController = new MainController();

        filterData = new JSONArray();
        String leido = Utils.readFromFile(ServerUtilities.context,"Account");
        try {
            ServerUtilities.jsonCustomer = new JSONObject(leido);
            txtProgreso.setText("Progreso: " + ServerUtilities.jsonCustomer.getJSONArray("records").length() + " clientes completados");
            listView.setAdapter(new CustomerAdapter(ServerUtilities.context,ServerUtilities.jsonCustomer.getJSONArray("records")));
            listView.requestFocus();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    Log.d("SELECTED","Seleccionada la posición " + position + " de la lista");
                    FragmentManager fm = getFragmentManager();
                    RouteListFragment routeDialog = new RouteListFragment();
                    routeDialog.setRetainInstance(true);
                    routeDialog.addSpecialParam(CustomerDashboardActivity.this);
                    routeDialog.show(fm, "menu");

                }
            });

        } catch (Exception e) {

        }



        et = (EditText) findViewById(R.id.editTextSearch);
        et.addTextChangedListener(new TextWatcher()
        {
            public void afterTextChanged(Editable s)
            {
                // Abstract Method of TextWatcher Interface.
            }
            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after)
            {
                // Abstract Method of TextWatcher Interface.
            }
            public void onTextChanged(CharSequence s,
                                      int start, int before, int count)
            {
                if (et.getText().length()==0) {
                    try {
                        if(filterData!=null) filterData = new JSONArray();
                        listView.setAdapter(new CustomerAdapter(ServerUtilities.context,ServerUtilities.jsonCustomer.getJSONArray("records")));
                        txtProgreso.setText("Progreso: " + ServerUtilities.jsonCustomer.getJSONArray("records").length() + " clientes completados");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return;
                } else {

                    int textlength = et.getText().length();
                    int listSize = 0;
                    try {
                        listSize = ServerUtilities.jsonCustomer.getJSONArray("records").length();
                        for (int i = 0; i < listSize; i++)
                        {
                            JSONObject item = new JSONObject();
                            try {
                                item = ServerUtilities.jsonCustomer.getJSONArray("records").optJSONObject(i);
                            } catch (Exception e) {

                            }
                            String code = Utils.getNotNullString(item.getString("SAP_code__c"));
                            String name = Utils.getNotNullString(item.getString("Name"));
                            if (textlength <= code.length() || textlength <= name.length())
                            {
                                boolean found = code.toLowerCase().contains(et.getText().toString().toLowerCase());
                                found = found || name.toLowerCase().contains(et.getText().toString().toLowerCase());
                                if (found)
                                {
                                    filterData.put(item);
                                }
                            }
                        }

                        listView.setAdapter(new CustomerAdapter(ServerUtilities.context,filterData));
                        txtProgreso.setText("Progreso: " + filterData.length() + " clientes completados");
                    } catch (Exception e) {
                        listSize = 0;
                        try {
                            if(filterData!=null) filterData = new JSONArray();
                            txtProgreso.setText("Progreso: " + ServerUtilities.jsonCustomer.getJSONArray("records").length() + " clientes completados");
                            listView.setAdapter(new CustomerAdapter(ServerUtilities.context,ServerUtilities.jsonCustomer.getJSONArray("records")));
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public void openFragmentMenu() {
        FragmentManager fm = getFragmentManager();
        CustomerMenuFragment routeDialog = new CustomerMenuFragment();
        routeDialog.setRetainInstance(true);
        routeDialog.show(fm, "menu");
    }

    public void refreshLocalData() throws UnsupportedEncodingException {
        sendRequestAccount(ServerUtilities.sqlAccount);
        sendRequestInquiry(ServerUtilities.sqlInquiry);
        sendRequestQuestion(ServerUtilities.sqlQuestion);
        sendRequestAnswer(ServerUtilities.sqlAnswer);
        sendRequestQuestionAnswer(ServerUtilities.sqlQuestionAnswer);
        sendRequestTargetAudience(ServerUtilities.sqlTargetAudiences);
        sendRequestPayment(ServerUtilities.sqlPaumentPending);
        sendRequestRoute(ServerUtilities.sqlRoute);
    }




    @Override
    public void onResume() {

        super.onResume();


        imgLocation = (ImageView) findViewById(R.id.imageMapa);
        this.imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = MainController.getIntentMapBoard(ServerUtilities.context);
                startActivity(i);
            }
        });

        imgLista = (ImageView) findViewById(R.id.imageLista);
        this.imgLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = MainController.getIntentMapBoard(ServerUtilities.context);
                startActivity(i);
            }
        });


        imgFiltro = (ImageView) findViewById(R.id.imageView4);
        this.imgFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                FragmentManager fm = getFragmentManager();
                CustomerMenuFragment testDialog = new CustomerMenuFragment();
                testDialog.setRetainInstance(true);
                testDialog.show(fm, "filtro");
            }
        });

        btnTodos = (Button) findViewById(R.id.btnTodo);
        this.btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                try {
                    refreshLocalData();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        btnProgreso = (Button) findViewById(R.id.btnEnProgreso);
        this.btnProgreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnPendiente = (Button) findViewById(R.id.btnPendiente);
        this.btnPendiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                //Intent i = new Intent(CustomerDashboardActivity.this, CustomerMapActivity.class);
                //startActivity(i);
            }
        });

        btnCompletos = (Button) findViewById(R.id.btnCompleto);
        this.btnCompletos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = new Intent(CustomerDashboardActivity.this, EditCustomerActivity.class);
                startActivity(i);
            }
        });

        imgRefresh = (ImageView) findViewById(R.id.imageView2);
        this.imgRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                try {
                    refreshLocalData();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

        imgAdd   = (ImageView) findViewById(R.id.imageView5);
        this.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = new Intent(CustomerDashboardActivity.this, EditCustomerActivity.class);
                startActivity(i);
            }
        });

    }


    @Override
    public void onResume(RestClient client) {
        // Keeping reference to rest client
        this.client = client;

        // Show everything
        //findViewById(R.id.root).setVisibility(View.VISIBLE);

        RestClient.ClientInfo clientInfo = client.getClientInfo();
        RestClient.ClientInfo newClientInfo = null;
        try {
            URL url = new URL("https://desarrollo-cbc-dev.cs67.force.com"); //Some instantiated URL object
            URI uri = url.toURI();
            url = new URL("https://test.salesforce.com/id/00D0n000000Cns8EAC/0050n000000HkOjAAK");
            URI idURI = url.toURI();
            url = new URL(ServerUtilities.appURL);
            URI loginURI = url.toURI();

        } catch (Exception e) {

        }
        Log.e("CInfo", client.getClientInfo().toString());

        Log.e("REFRESH TOKEN", client.getRefreshToken());
    }


    /**
     * Called when "Logout" button is clicked.
     *
     * @param v
     */
    public void onLogoutClick(View v) {
        SalesforceSDKManager.getInstance().logout(this);
    }


    private void sendRequestAccount(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Account",ServerUtilities.context);
                            //Log.d("ACCOUNT","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Account");
                            Log.d("ACCOUNT","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    private void sendRequestInquiry(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Inquiry",ServerUtilities.context);
                            //Log.d("INQUIRY","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Inquiry");
                            Log.d("INQUIRY","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void sendRequestQuestion(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Question",ServerUtilities.context);
                            //Log.d("QUESTION","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Question");
                            Log.d("QUESTION","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    private void sendRequestAnswer(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Answer",ServerUtilities.context);
                            //Log.d("ANSWER","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Answer");
                            Log.d("ANSWER","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void sendRequestQuestionAnswer(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"QuestionAnswer",ServerUtilities.context);
                            //Log.d("Q-ANSWER","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"QuestionAnswer");
                            Log.d("Q-ANSWER","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    private void sendRequestTargetAudience(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"TargetAudience",ServerUtilities.context);
                            //Log.d("TARGET","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"TargetAudience");
                            Log.d("TARGET","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    private void sendRequestPayment(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Payment",ServerUtilities.context);
                            //Log.d("TARGET","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Payment");
                            Log.d("TARGET","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    private void sendRequestRoute(String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), soql);

        client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String leido;
                            //Log.e("RESULT",result.toString());
                            Utils.writeToFile(result.toString(),"Route",ServerUtilities.context);
                            //Log.d("TARGET","Saved");
                            leido = Utils.readFromFile(ServerUtilities.context,"Route");
                            Log.d("ROUTE","READ: " + leido);
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CustomerDashboardActivity.this,
                                CustomerDashboardActivity.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), exception.toString()),
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

}

