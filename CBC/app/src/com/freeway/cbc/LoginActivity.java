package com.freeway.cbc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.freeway.cbc.domain.User;
import com.freeway.cbc.util.AlertDialogManager;
import com.freeway.cbc.util.Login;
import com.freeway.cbc.util.LoginListener;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.app.SalesforceSDKManager.KeyInterface;
import com.salesforce.androidsdk.analytics.security.Encryptor;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class LoginActivity extends Activity {

    private Button loginButton;
    private EditText usuario;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        SalesforceSDKManager.initNative(getApplicationContext(), new NativeKeyImpl(), MainActivity.class);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_login);


        this.usuario = (EditText) findViewById(R.id.editTextLoginUserName);
        this.password = (EditText) findViewById(R.id.editTextLoginPassword);

        this.usuario.setText("armandoafacbc@freewayconsulting.com");
        this.password.setText("cbc2017cbcMaQ5LBHI033pwFho5igDrepK");

        this.loginButton = (Button) findViewById(R.id.buttonLoginOk);
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                logon();
            }
        });

    }

    private  void logon()
    {
        final String userName = this.usuario.getText().toString();
        final String userpassword = this.password.getText().toString();

        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.logingtext));
        progressDialog.show();

        if (userName.length() == 0 || userpassword.length() == 0)
        {
            new AlertDialogManager().showAlertDialog(LoginActivity.this,
                    getResources().getString(R.string.error),
                    getResources().getString(R.string.error_logon_user),
                    ContextCompat.getDrawable(getApplicationContext(), R.drawable.cbc_icon),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                        }
                    });
            progressDialog.dismiss();
            return;
        }

        Login.login(this, userName, userpassword, new LoginListener() {
            @Override
            public void onEnded(User user)
            {
                //SessionManager sessionManager = new SessionManager(LoginActivity.this);
                //sessionManager.createLoginSession(user.userId, userName, userpassword, uuid,
                //        user.clientId, user.name, user.rol, user.rolId, user.bussinesId);

                progressDialog.dismiss();
                Log.d("Login", "Mostrar MainActivity");
                Intent i = new Intent(LoginActivity.this, CustomerDashboardActivity.class);
                startActivity(i);

                Log.d("Login", "Cerrar Login");
                LoginActivity.this.finish();
            }

            @Override
            public void onError() {
                progressDialog.dismiss();
            }

            @Override
            public void onCancel() {
                progressDialog.dismiss();
            }
        });

    }

}


