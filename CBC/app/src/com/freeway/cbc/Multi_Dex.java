package com.freeway.cbc;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by macbookpro on 04/09/17.
 */

public class Multi_Dex  extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
