package com.freeway.cbc;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.freeway.cbc.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class CustomerAdapter extends BaseAdapter {
    LayoutInflater inflater;
    ViewHolder viewHolder;

    int selectedPos = -1;	// init value for not-selected



    public JSONArray jsonList = new JSONArray();


    public CustomerAdapter(Context context, JSONArray ja) {
        // TODO Auto-generated constructor stub
        inflater = LayoutInflater.from(context);
        jsonList = ja;
    }

    public CustomerAdapter(TextWatcher textWatcher) {
        // TODO Auto-generated constructor stub
    }


    public void setSelectedPosition(int pos){
        selectedPos = pos;
        // inform the view of this change
        notifyDataSetChanged();
    }

    public int getSelectedPosition(){
        return selectedPos;
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return jsonList.length();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.activity_customer_adapter, null);
            viewHolder = new ViewHolder();


            viewHolder.txt_codigo = (TextView) convertView.findViewById(R.id.textViewCodigo);
            viewHolder.txt_nombre = (TextView) convertView.findViewById(R.id.textViewNombre);
            viewHolder.txt_route = (TextView) convertView.findViewById(R.id.textViewRuta);
            viewHolder.txt_address = (TextView) convertView.findViewById(R.id.textViewDireccion);
            viewHolder.txt_clientType = (TextView) convertView.findViewById((R.id.textViewTipoCliente));
            viewHolder.txt_status = (TextView) convertView.findViewById(R.id.textViewStatus);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JSONObject item = null;
        try {
            item = jsonList.optJSONObject(position);
            JSONObject atributo = item.getJSONObject("attributes");
            viewHolder.txt_codigo.setText(" " + Utils.getNotNullString(item.getString("SAP_code__c")));
            viewHolder.txt_nombre.setText(" " + Utils.getNotNullString(item.getString("Name")));
            viewHolder.txt_route.setText("Ruta: " + Utils.getNotNullString(item.getString("Route__c")));
            String address = Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("street"));
            address = address + ", " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("city"));
            address = address + ", CP.: " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("postalCode"));
            viewHolder.txt_address.setText("Dirección: " + address);
            viewHolder.txt_clientType.setText("Tipo CLiente: " + Utils.getNotNullString(item.getString("Account_type__c")));
            viewHolder.txt_status.setText("Estado: Pendiente");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;

    }

/*
[{"attributes":{"type":"Account","url":"/services/data/v39.0/sobjects/Account/0010n000008FWN1AAO"},"SAP_code__c":"0","Description":null,"Route__c":0.0,
"ShippingAddress":{"city":null,"country":"Guatemala","countryCode":null,"geocodeAccuracy":null,"latitude":null,"longitude":null,"postalCode":null,"state":null,"stateCode":null,"street":null},
"BillingAddress":null,"Channel__c":"a010n0000017MRmAAM","Subchannel__c":"a020n000001SSxCAAW","Labeled__c":"a060n000001CVbnAAG",
"Account_type__c":"Supermercado","Share__c":null,"Refrigeration_model__c":"a050n000001rH2MAAU"}
*/
    private class ViewHolder {
        TextView txt_codigo;
        TextView txt_nombre;
        TextView txt_route;
        TextView txt_address;
        TextView txt_clientType;
        TextView txt_status;

    }

}
