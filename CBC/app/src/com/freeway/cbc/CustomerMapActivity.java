package com.freeway.cbc;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.freeway.cbc.controller.MainController;
import com.freeway.cbc.util.ServerUtilities;
import com.freeway.cbc.util.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class CustomerMapActivity extends AppCompatActivity  implements OnMapReadyCallback {

    SupportMapFragment mapFragment;

    private Button btnTodos;
    private Button btnPendiente;
    private Button btnProgreso;
    private Button btnCompletos;

    private ImageView imgFiltro;
    private ImageView imgAdd;

    private MapView mapView;

    private ImageView imgLocation;
    private ImageView imgLista;
    private EditText et;

    private ListView listView;

    private GoogleMap googleMap;

    private JSONObject oData;
    private JSONArray filterData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_customer_map);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

                        JSONArray jsonArray = new JSONArray();
                        try {
                            jsonArray = ServerUtilities.jsonCustomer.getJSONArray("records");
                            for(int i=0; i<jsonArray.length();i++) {
                                JSONObject item = jsonArray.getJSONObject(i);
                                String title = Utils.getNotNullString(item.getString("Name"));
                                String address = Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("street"));
                                address = address + ", " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("city"));
                                address = address + ", CP.: " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("postalCode"));
                                Double lat = Utils.getNotNullDouble(item.getJSONObject("ShippingAddress").getDouble("latitude"));
                                Double lng = Utils.getNotNullDouble(item.getJSONObject("ShippingAddress").getDouble("longitude"));
                                if(lat!=0.0d  && lng!=0.0d) {
                                    googleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat, lng))
                                            .title(title)
                                            .snippet(address));
                                    if(i==jsonArray.length()-1)
                                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10));  //Esto es TEMPORAL
                                }
                            }

                        } catch (JSONException e) {


                        }



                    }
                });
            }
        });




    }

    private void updateMarkers(){



/*
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4233438, -122.0728817))
                .title("Tienda La Princesita, Dirección 1, Teléfono 12345678")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4629101,-122.2449094))
                .title("Facebook")
                .snippet("WallMart Centro, Dirección 2, Teléfono 98765432"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.3092293, -122.1136845))
                .title("Abarrotera Milennium, Dirección 3, Teléfono 45678901"));

        viewHolder.txt_nombre.setText(" " + Utils.getNotNullString(item.getString("Name")));
        viewHolder.txt_route.setText("Ruta: " + Utils.getNotNullString(item.getString("Route__c")));
        String address = Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("street"));
        address = address + ", " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("city"));
        address = address + ", CP.: " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("postalCode"));
        viewHolder.txt_address.setText("Dirección: " + address);
*/
    }

    @Override
    public void onResume() {

        super.onResume();

        imgLocation = (ImageView) findViewById(R.id.imageMapa);
        this.imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = MainController.getIntentDashBoard(ServerUtilities.context);
                startActivity(i);
            }
        });

        imgLista = (ImageView) findViewById(R.id.imageLista);
        this.imgLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = MainController.getIntentDashBoard(ServerUtilities.context);
                startActivity(i);
            }
        });

        btnTodos = (Button) findViewById(R.id.btnTodo);
        this.btnTodos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                //try {
                //} catch (UnsupportedEncodingException e) {
                //    e.printStackTrace();
                //}
            }
        });

        btnProgreso = (Button) findViewById(R.id.btnEnProgreso);
        this.btnProgreso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnPendiente = (Button) findViewById(R.id.btnPendiente);
        this.btnPendiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                //Intent i = new Intent(CustomerDashboardActivity.this, CustomerMapActivity.class);
                //startActivity(i);
            }
        });

        btnCompletos = (Button) findViewById(R.id.btnCompleto);
        this.btnCompletos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = new Intent(CustomerMapActivity.this, EditCustomerActivity.class);
                startActivity(i);
            }
        });

        imgFiltro = (ImageView) findViewById(R.id.imageView4);
        this.imgFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                FragmentManager fm = getFragmentManager();
                DialogFiltroFragment testDialog = new DialogFiltroFragment();
                testDialog.setRetainInstance(true);
                testDialog.show(fm, "filtro");
            }
        });

        imgAdd   = (ImageView) findViewById(R.id.imageView5);
        this.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
                Intent i = new Intent(CustomerMapActivity.this, EditCustomerActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        JSONArray jsonArray = new JSONArray();
        try {
            Double lastlat=0.0d, lastlng=0.0d;

            jsonArray = ServerUtilities.jsonCustomer.getJSONArray("records");
            for(int i=0; i<jsonArray.length();i++) {
                JSONObject item = jsonArray.getJSONObject(i);
                String title = Utils.getNotNullString(item.getString("Name"));
                String address = Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("street"));
                address = address + ", " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("city"));
                address = address + ", CP.: " + Utils.getNotNullString(item.getJSONObject("ShippingAddress").getString("postalCode"));
                Double lat = Utils.getNotNullDouble(item.getJSONObject("ShippingAddress").getDouble("latitude"));
                Double lng = Utils.getNotNullDouble(item.getJSONObject("ShippingAddress").getDouble("longitude"));
                //if(lat>2.0d  && lng>2.0d) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(title)
                            .snippet(address));
                    lastlat = lat;
                    lastlng = lng;
                //}
            }
            if(lastlat!=0.0d)
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastlat, lastlng), 10));  //Esto es TEMPORAL

        } catch (JSONException e) {


        }



        /*
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4233438, -122.0728817))
                .title("LinkedIn")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4629101,-122.2449094))
                .title("Facebook")
                .snippet("Facebook HQ: Menlo Park"));

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.3092293, -122.1136845))
                .title("Apple"));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.4233438, -122.0728817), 10));
        */
    }
}
