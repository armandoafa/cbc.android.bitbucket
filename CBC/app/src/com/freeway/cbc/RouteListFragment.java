package com.freeway.cbc;

import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.freeway.cbc.controller.MainController;
import com.freeway.cbc.util.ServerUtilities;
import com.freeway.cbc.util.Utils;

import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RouteListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RouteListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RouteListFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private CustomerDashboardActivity dashboard;

    private RadioButton rb0;
    private RadioButton rb1;
    private RadioButton rb2;


    private String route;



    public RouteListFragment() {
        // Required empty public constructor
    }

    public void addSpecialParam(CustomerDashboardActivity a)  {
        this.dashboard = a;
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RouteListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RouteListFragment newInstance(String param1, String param2) {
        RouteListFragment fragment = new RouteListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_route_list, container, false);
        // Inflate the layout for this fragment
        rb0 = (RadioButton) view.findViewById(R.id.radio0);
        rb1 = (RadioButton) view.findViewById(R.id.radio1);
        rb2 = (RadioButton) view.findViewById(R.id.radio2);

        String leido = Utils.readFromFile(ServerUtilities.context,"Route");
        try {
            ServerUtilities.jsonRoute = new JSONObject(leido);
            route = ServerUtilities.jsonRoute.getJSONArray("records").getJSONObject(0).getString("Name");
            rb0.setText(route);
            rb0.setOnClickListener( new View.OnClickListener (){
                public void onClick(View v) {
                    MainController.currentRoute = route;
                    dashboard.openFragmentMenu();
                    dismiss();
                }
            });
            route = ServerUtilities.jsonRoute.getJSONArray("records").getJSONObject(1).getString("Name");
            rb1.setText(route);
            rb1.setOnClickListener( new View.OnClickListener (){
                public void onClick(View v) {
                    MainController.currentRoute = route;
                }
            });
            route = ServerUtilities.jsonRoute.getJSONArray("records").getJSONObject(2).getString("Name");
            rb2.setText(route);
            rb2.setOnClickListener( new View.OnClickListener (){
                public void onClick(View v) {
                    MainController.currentRoute = route;
                }
            });

        } catch (Exception e) {

        }

        return view;  //inflater.inflate(R.layout.fragment_route_list, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
