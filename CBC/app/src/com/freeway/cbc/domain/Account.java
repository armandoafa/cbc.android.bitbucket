package com.freeway.cbc.domain;

/**
 * Created by macbookpro on 31/08/17.
 */

public class Account {

    private String AccountContactRoles;
    private String AccountPartnersFrom;
    private String AccountPartnersTo;
    private String AccountSource;
    private String ActivityHistories;
    private String Assets;
    private String AttachedContentDocuments;
    private String AttachedContentNotes;
    private String Attachments;
    private String BillingAddress;
    private String BillingCity;
    private String BillingCountry;
    private String BillingGeocodeAccuracy;
    private String BillingLatitude;
    private String BillingLongitude;
    private String BillingPostalCode;
    private String BillingState;
    private String BillingStreet;
    private String Cases;
    private String ChildAccounts;
    private String CombinedAttachments;
    private String Contacts;
    private String ContentDocumentLinks;
    private String Contracts;
    private String CreatedBy;
    private String CreatedById;
    private String CreatedDate;
    private String Description;
    private String DuplicateRecordItems;
    private String Emails;
    private String EventRelations;
    private String Events;
    private String FeedSubscriptionsForEntity;
    private String Feeds;
    private String Histories;
    private String Industry;
    private String IsDeleted;
    private String Jigsaw;
    private String JigsawCompanyId;
    private String LastActivityDate;
    private String LastModifiedBy;
    private String LastModifiedById;
    private String LastModifiedDate;
    private String LastReferencedDate;
    private String LastViewedDate;
    private String LookedUpFromActivities;
    private String MasterRecord;
    private String MasterRecordId;
    private String Name;
    private String Notes;
    private String NotesAndAttachments;
    private String NumberOfEmployees;
    private String OpenActivities ;
    private String Opportunities ;
    private String OpportunityPartnersTo ;
    private String Orders ;
    private String Owner;  // minOccurs="0" type="ens:User"/>
    private String OwnerId;  // minOccurs="0" type="tns:ID"/>
    private String Parent;  // minOccurs="0" type="ens:Account"/>
    private String ParentId;  // minOccurs="0" type="tns:ID"/>
    private String PartnersFrom ;
    private String PartnersTo ;
    private String Phone;  // minOccurs="0" type="xsd:string"/>
    private String PhotoUrl;  // minOccurs="0" type="xsd:string"/>
    private String ProcessInstances ;
    private String ProcessSteps ;
    private String RecordAssociatedGroups ;
    private String Shares ;
    private String ShippingAddress;  // minOccurs="0" type="tns:address"/>
    private String ShippingCity;  // minOccurs="0" type="xsd:string"/>
    private String ShippingCountry;  // minOccurs="0" type="xsd:string"/>
    private String ShippingGeocodeAccuracy;  // minOccurs="0" type="xsd:string"/>
    private String ShippingLatitude;  // minOccurs="0" type="xsd:double"/>
    private String ShippingLongitude;  // minOccurs="0" type="xsd:double"/>
    private String ShippingPostalCode;  // minOccurs="0" type="xsd:string"/>
    private String ShippingState;  // minOccurs="0" type="xsd:string"/>
    private String ShippingStreet;  // minOccurs="0" type="xsd:string"/>
    private String SicDesc;  // minOccurs="0" type="xsd:string"/>
    private String SystemModstamp;  // minOccurs="0" type="xsd:dateTime"/>
    private String TaskRelations ;
    private String Tasks ;
    private String TopicAssignments ;
    private String Type;  // minOccurs="0" type="xsd:string"/>
    private String UserRecordAccess;  // minOccurs="0" type="ens:UserRecordAccess"/>
    private String Website;  // minOccurs="0" type="xsd:string"/>

    public Account(String accountContactRoles, String accountPartnersFrom, String accountPartnersTo, String accountSource, String activityHistories,
                   String assets, String attachedContentDocuments, String attachedContentNotes, String attachments, String billingAddress, String billingCity,
                   String billingCountry, String billingGeocodeAccuracy, String billingLatitude, String billingLongitude, String billingPostalCode,
                   String billingState, String billingStreet, String cases, String childAccounts, String combinedAttachments, String contacts,
                   String contentDocumentLinks, String contracts, String createdBy, String createdById, String createdDate, String description,
                   String duplicateRecordItems, String emails, String eventRelations, String events, String feedSubscriptionsForEntity, String feeds,
                   String histories, String industry, String isDeleted, String jigsaw, String jigsawCompanyId, String lastActivityDate, String lastModifiedBy,
                   String lastModifiedById, String lastModifiedDate, String lastReferencedDate, String lastViewedDate, String lookedUpFromActivities,
                   String masterRecord, String masterRecordId, String name, String notes, String notesAndAttachments, String numberOfEmployees,
                   String openActivities, String opportunities, String opportunityPartnersTo, String orders, String owner, String ownerId,
                   String parent, String parentId, String partnersFrom, String partnersTo, String phone, String photoUrl, String processInstances,
                   String processSteps, String recordAssociatedGroups, String shares, String shippingAddress, String shippingCity, String shippingCountry,
                   String shippingGeocodeAccuracy, String shippingLatitude, String shippingLongitude, String shippingPostalCode, String shippingState,
                   String shippingStreet, String sicDesc, String systemModstamp, String taskRelations, String tasks, String topicAssignments,
                   String type, String userRecordAccess, String website) {

        AccountContactRoles = accountContactRoles;
        AccountPartnersFrom = accountPartnersFrom;
        AccountPartnersTo = accountPartnersTo;
        AccountSource = accountSource;
        ActivityHistories = activityHistories;
        Assets = assets;
        AttachedContentDocuments = attachedContentDocuments;
        AttachedContentNotes = attachedContentNotes;
        Attachments = attachments;
        BillingAddress = billingAddress;
        BillingCity = billingCity;
        BillingCountry = billingCountry;
        BillingGeocodeAccuracy = billingGeocodeAccuracy;
        BillingLatitude = billingLatitude;
        BillingLongitude = billingLongitude;
        BillingPostalCode = billingPostalCode;
        BillingState = billingState;
        BillingStreet = billingStreet;
        Cases = cases;
        ChildAccounts = childAccounts;
        CombinedAttachments = combinedAttachments;
        Contacts = contacts;
        ContentDocumentLinks = contentDocumentLinks;
        Contracts = contracts;
        CreatedBy = createdBy;
        CreatedById = createdById;
        CreatedDate = createdDate;
        Description = description;
        DuplicateRecordItems = duplicateRecordItems;
        Emails = emails;
        EventRelations = eventRelations;
        Events = events;
        FeedSubscriptionsForEntity = feedSubscriptionsForEntity;
        Feeds = feeds;
        Histories = histories;
        Industry = industry;
        IsDeleted = isDeleted;
        Jigsaw = jigsaw;
        JigsawCompanyId = jigsawCompanyId;
        LastActivityDate = lastActivityDate;
        LastModifiedBy = lastModifiedBy;
        LastModifiedById = lastModifiedById;
        LastModifiedDate = lastModifiedDate;
        LastReferencedDate = lastReferencedDate;
        LastViewedDate = lastViewedDate;
        LookedUpFromActivities = lookedUpFromActivities;
        MasterRecord = masterRecord;
        MasterRecordId = masterRecordId;
        Name = name;
        Notes = notes;
        NotesAndAttachments = notesAndAttachments;
        NumberOfEmployees = numberOfEmployees;
        OpenActivities = openActivities;
        Opportunities = opportunities;
        OpportunityPartnersTo = opportunityPartnersTo;
        Orders = orders;
        Owner = owner;
        OwnerId = ownerId;
        Parent = parent;
        ParentId = parentId;
        PartnersFrom = partnersFrom;
        PartnersTo = partnersTo;
        Phone = phone;
        PhotoUrl = photoUrl;
        ProcessInstances = processInstances;
        ProcessSteps = processSteps;
        RecordAssociatedGroups = recordAssociatedGroups;
        Shares = shares;
        ShippingAddress = shippingAddress;
        ShippingCity = shippingCity;
        ShippingCountry = shippingCountry;
        ShippingGeocodeAccuracy = shippingGeocodeAccuracy;
        ShippingLatitude = shippingLatitude;
        ShippingLongitude = shippingLongitude;
        ShippingPostalCode = shippingPostalCode;
        ShippingState = shippingState;
        ShippingStreet = shippingStreet;
        SicDesc = sicDesc;
        SystemModstamp = systemModstamp;
        TaskRelations = taskRelations;
        Tasks = tasks;
        TopicAssignments = topicAssignments;
        Type = type;
        UserRecordAccess = userRecordAccess;
        Website = website;
    }

    public String getAccountContactRoles() {
        return AccountContactRoles;
    }

    public void setAccountContactRoles(String accountContactRoles) {
        AccountContactRoles = accountContactRoles;
    }

    public String getAccountPartnersFrom() {
        return AccountPartnersFrom;
    }

    public void setAccountPartnersFrom(String accountPartnersFrom) {
        AccountPartnersFrom = accountPartnersFrom;
    }

    public String getAccountPartnersTo() {
        return AccountPartnersTo;
    }

    public void setAccountPartnersTo(String accountPartnersTo) {
        AccountPartnersTo = accountPartnersTo;
    }

    public String getAccountSource() {
        return AccountSource;
    }

    public void setAccountSource(String accountSource) {
        AccountSource = accountSource;
    }

    public String getActivityHistories() {
        return ActivityHistories;
    }

    public void setActivityHistories(String activityHistories) {
        ActivityHistories = activityHistories;
    }

    public String getAssets() {
        return Assets;
    }

    public void setAssets(String assets) {
        Assets = assets;
    }

    public String getAttachedContentDocuments() {
        return AttachedContentDocuments;
    }

    public void setAttachedContentDocuments(String attachedContentDocuments) {
        AttachedContentDocuments = attachedContentDocuments;
    }

    public String getAttachedContentNotes() {
        return AttachedContentNotes;
    }

    public void setAttachedContentNotes(String attachedContentNotes) {
        AttachedContentNotes = attachedContentNotes;
    }

    public String getAttachments() {
        return Attachments;
    }

    public void setAttachments(String attachments) {
        Attachments = attachments;
    }

    public String getBillingAddress() {
        return BillingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        BillingAddress = billingAddress;
    }

    public String getBillingCity() {
        return BillingCity;
    }

    public void setBillingCity(String billingCity) {
        BillingCity = billingCity;
    }

    public String getBillingCountry() {
        return BillingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        BillingCountry = billingCountry;
    }

    public String getBillingGeocodeAccuracy() {
        return BillingGeocodeAccuracy;
    }

    public void setBillingGeocodeAccuracy(String billingGeocodeAccuracy) {
        BillingGeocodeAccuracy = billingGeocodeAccuracy;
    }

    public String getBillingLatitude() {
        return BillingLatitude;
    }

    public void setBillingLatitude(String billingLatitude) {
        BillingLatitude = billingLatitude;
    }

    public String getBillingLongitude() {
        return BillingLongitude;
    }

    public void setBillingLongitude(String billingLongitude) {
        BillingLongitude = billingLongitude;
    }

    public String getBillingPostalCode() {
        return BillingPostalCode;
    }

    public void setBillingPostalCode(String billingPostalCode) {
        BillingPostalCode = billingPostalCode;
    }

    public String getBillingState() {
        return BillingState;
    }

    public void setBillingState(String billingState) {
        BillingState = billingState;
    }

    public String getBillingStreet() {
        return BillingStreet;
    }

    public void setBillingStreet(String billingStreet) {
        BillingStreet = billingStreet;
    }

    public String getCases() {
        return Cases;
    }

    public void setCases(String cases) {
        Cases = cases;
    }

    public String getChildAccounts() {
        return ChildAccounts;
    }

    public void setChildAccounts(String childAccounts) {
        ChildAccounts = childAccounts;
    }

    public String getCombinedAttachments() {
        return CombinedAttachments;
    }

    public void setCombinedAttachments(String combinedAttachments) {
        CombinedAttachments = combinedAttachments;
    }

    public String getContacts() {
        return Contacts;
    }

    public void setContacts(String contacts) {
        Contacts = contacts;
    }

    public String getContentDocumentLinks() {
        return ContentDocumentLinks;
    }

    public void setContentDocumentLinks(String contentDocumentLinks) {
        ContentDocumentLinks = contentDocumentLinks;
    }

    public String getContracts() {
        return Contracts;
    }

    public void setContracts(String contracts) {
        Contracts = contracts;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getCreatedById() {
        return CreatedById;
    }

    public void setCreatedById(String createdById) {
        CreatedById = createdById;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDuplicateRecordItems() {
        return DuplicateRecordItems;
    }

    public void setDuplicateRecordItems(String duplicateRecordItems) {
        DuplicateRecordItems = duplicateRecordItems;
    }

    public String getEmails() {
        return Emails;
    }

    public void setEmails(String emails) {
        Emails = emails;
    }

    public String getEventRelations() {
        return EventRelations;
    }

    public void setEventRelations(String eventRelations) {
        EventRelations = eventRelations;
    }

    public String getEvents() {
        return Events;
    }

    public void setEvents(String events) {
        Events = events;
    }

    public String getFeedSubscriptionsForEntity() {
        return FeedSubscriptionsForEntity;
    }

    public void setFeedSubscriptionsForEntity(String feedSubscriptionsForEntity) {
        FeedSubscriptionsForEntity = feedSubscriptionsForEntity;
    }

    public String getFeeds() {
        return Feeds;
    }

    public void setFeeds(String feeds) {
        Feeds = feeds;
    }

    public String getHistories() {
        return Histories;
    }

    public void setHistories(String histories) {
        Histories = histories;
    }

    public String getIndustry() {
        return Industry;
    }

    public void setIndustry(String industry) {
        Industry = industry;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getJigsaw() {
        return Jigsaw;
    }

    public void setJigsaw(String jigsaw) {
        Jigsaw = jigsaw;
    }

    public String getJigsawCompanyId() {
        return JigsawCompanyId;
    }

    public void setJigsawCompanyId(String jigsawCompanyId) {
        JigsawCompanyId = jigsawCompanyId;
    }

    public String getLastActivityDate() {
        return LastActivityDate;
    }

    public void setLastActivityDate(String lastActivityDate) {
        LastActivityDate = lastActivityDate;
    }

    public String getLastModifiedBy() {
        return LastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        LastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedById() {
        return LastModifiedById;
    }

    public void setLastModifiedById(String lastModifiedById) {
        LastModifiedById = lastModifiedById;
    }

    public String getLastModifiedDate() {
        return LastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        LastModifiedDate = lastModifiedDate;
    }

    public String getLastReferencedDate() {
        return LastReferencedDate;
    }

    public void setLastReferencedDate(String lastReferencedDate) {
        LastReferencedDate = lastReferencedDate;
    }

    public String getLastViewedDate() {
        return LastViewedDate;
    }

    public void setLastViewedDate(String lastViewedDate) {
        LastViewedDate = lastViewedDate;
    }

    public String getLookedUpFromActivities() {
        return LookedUpFromActivities;
    }

    public void setLookedUpFromActivities(String lookedUpFromActivities) {
        LookedUpFromActivities = lookedUpFromActivities;
    }

    public String getMasterRecord() {
        return MasterRecord;
    }

    public void setMasterRecord(String masterRecord) {
        MasterRecord = masterRecord;
    }

    public String getMasterRecordId() {
        return MasterRecordId;
    }

    public void setMasterRecordId(String masterRecordId) {
        MasterRecordId = masterRecordId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getNotesAndAttachments() {
        return NotesAndAttachments;
    }

    public void setNotesAndAttachments(String notesAndAttachments) {
        NotesAndAttachments = notesAndAttachments;
    }

    public String getNumberOfEmployees() {
        return NumberOfEmployees;
    }

    public void setNumberOfEmployees(String numberOfEmployees) {
        NumberOfEmployees = numberOfEmployees;
    }

    public String getOpenActivities() {
        return OpenActivities;
    }

    public void setOpenActivities(String openActivities) {
        OpenActivities = openActivities;
    }

    public String getOpportunities() {
        return Opportunities;
    }

    public void setOpportunities(String opportunities) {
        Opportunities = opportunities;
    }

    public String getOpportunityPartnersTo() {
        return OpportunityPartnersTo;
    }

    public void setOpportunityPartnersTo(String opportunityPartnersTo) {
        OpportunityPartnersTo = opportunityPartnersTo;
    }

    public String getOrders() {
        return Orders;
    }

    public void setOrders(String orders) {
        Orders = orders;
    }

    public String getOwner() {
        return Owner;
    }

    public void setOwner(String owner) {
        Owner = owner;
    }

    public String getOwnerId() {
        return OwnerId;
    }

    public void setOwnerId(String ownerId) {
        OwnerId = ownerId;
    }

    public String getParent() {
        return Parent;
    }

    public void setParent(String parent) {
        Parent = parent;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public String getPartnersFrom() {
        return PartnersFrom;
    }

    public void setPartnersFrom(String partnersFrom) {
        PartnersFrom = partnersFrom;
    }

    public String getPartnersTo() {
        return PartnersTo;
    }

    public void setPartnersTo(String partnersTo) {
        PartnersTo = partnersTo;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getProcessInstances() {
        return ProcessInstances;
    }

    public void setProcessInstances(String processInstances) {
        ProcessInstances = processInstances;
    }

    public String getProcessSteps() {
        return ProcessSteps;
    }

    public void setProcessSteps(String processSteps) {
        ProcessSteps = processSteps;
    }

    public String getRecordAssociatedGroups() {
        return RecordAssociatedGroups;
    }

    public void setRecordAssociatedGroups(String recordAssociatedGroups) {
        RecordAssociatedGroups = recordAssociatedGroups;
    }

    public String getShares() {
        return Shares;
    }

    public void setShares(String shares) {
        Shares = shares;
    }

    public String getShippingAddress() {
        return ShippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        ShippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return ShippingCity;
    }

    public void setShippingCity(String shippingCity) {
        ShippingCity = shippingCity;
    }

    public String getShippingCountry() {
        return ShippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        ShippingCountry = shippingCountry;
    }

    public String getShippingGeocodeAccuracy() {
        return ShippingGeocodeAccuracy;
    }

    public void setShippingGeocodeAccuracy(String shippingGeocodeAccuracy) {
        ShippingGeocodeAccuracy = shippingGeocodeAccuracy;
    }

    public String getShippingLatitude() {
        return ShippingLatitude;
    }

    public void setShippingLatitude(String shippingLatitude) {
        ShippingLatitude = shippingLatitude;
    }

    public String getShippingLongitude() {
        return ShippingLongitude;
    }

    public void setShippingLongitude(String shippingLongitude) {
        ShippingLongitude = shippingLongitude;
    }

    public String getShippingPostalCode() {
        return ShippingPostalCode;
    }

    public void setShippingPostalCode(String shippingPostalCode) {
        ShippingPostalCode = shippingPostalCode;
    }

    public String getShippingState() {
        return ShippingState;
    }

    public void setShippingState(String shippingState) {
        ShippingState = shippingState;
    }

    public String getShippingStreet() {
        return ShippingStreet;
    }

    public void setShippingStreet(String shippingStreet) {
        ShippingStreet = shippingStreet;
    }

    public String getSicDesc() {
        return SicDesc;
    }

    public void setSicDesc(String sicDesc) {
        SicDesc = sicDesc;
    }

    public String getSystemModstamp() {
        return SystemModstamp;
    }

    public void setSystemModstamp(String systemModstamp) {
        SystemModstamp = systemModstamp;
    }

    public String getTaskRelations() {
        return TaskRelations;
    }

    public void setTaskRelations(String taskRelations) {
        TaskRelations = taskRelations;
    }

    public String getTasks() {
        return Tasks;
    }

    public void setTasks(String tasks) {
        Tasks = tasks;
    }

    public String getTopicAssignments() {
        return TopicAssignments;
    }

    public void setTopicAssignments(String topicAssignments) {
        TopicAssignments = topicAssignments;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getUserRecordAccess() {
        return UserRecordAccess;
    }

    public void setUserRecordAccess(String userRecordAccess) {
        UserRecordAccess = userRecordAccess;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }
}
