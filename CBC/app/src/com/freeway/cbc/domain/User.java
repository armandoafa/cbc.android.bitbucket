package com.freeway.cbc.domain;

/**
 * Created by macbookpro on 01/09/17.
 */

public class User {
 public String  id; //      "id":"https://login.salesforce.com/id/00D50000000IZ3ZEAW/00550000001fg5OAAQ",
 public String issued_at; //               "issued_at":"1296509381665",
 public String instance_url;         //      "instance_url":"https://na1.salesforce.com",
 public String signature;  //               "signature":"+Nbl5EOl/DlsvUZ4NbGDno6vn935XsWGVbwoKyXHayo=",
 public String access_token;  //               "access_token":"00D50000000IZ3Z!AQgAQH0Yd9M51BU_rayzAdmZ6NmT3pXZBgzkc3JTwDOGBl8BP2AREOiZzL_A2zg7etH81kTuuQPljJVsX4CPt3naL7qustlb"
}

/*
public String userName;
public String uuid;
public String bussinesId;
public int userId;
public int clientId;
public String bussines;
public String rolId;
public String rol;
public String email;
public String phone;
public String mobile;
public int status;
public String name;
*/