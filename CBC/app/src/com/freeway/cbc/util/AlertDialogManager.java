package com.freeway.cbc.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

/**
 * Created by macbookpro on 01/09/17.
 */

public class AlertDialogManager {

    public void showAlertDialog(Activity activity , String title, String message, Drawable icon/*, Boolean status*/)
    {
        showAlertDialog(activity , title, message, icon/*, Boolean status*/, null);
    }

    public void showAlertDialog(Activity activity , String title, String message/*, Boolean status*/,
                                Drawable icon, DialogInterface.OnClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        builder.setMessage(message);

        DialogInterface.OnClickListener clickListener = null;
        if (listener == null)
        {
            clickListener = new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            };
        }
        else
            clickListener = listener;

        builder.setPositiveButton("OK", clickListener);

        AlertDialog dialog = builder.create();

        dialog.setIcon(icon);

        dialog.show();
    }

    public void showAlertDialog(Activity activity , String title, String message/*, Boolean status*/,
                                Drawable icon, String buttonYesText, String buttonNoText,
                                DialogInterface.OnClickListener yesListener, DialogInterface.OnClickListener noListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);

        builder.setMessage(message);

        builder.setPositiveButton(buttonYesText, yesListener);

        builder.setNegativeButton(buttonNoText, noListener);

        AlertDialog dialog = builder.create();

        dialog.setIcon(icon);

        dialog.show();
    }
}
