package com.freeway.cbc.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by macbookpro on 01/09/17.
 */

public class JsonBase {

    final private String TAG = JsonBase.class.getName();

    protected boolean valid;
    protected String errorMessage;
    protected JSONObject json;

    protected JSONObject jsonCommand;
    protected  JSONObject jsonParam;

    protected JSONArray jsonCommandArray;
    protected JSONArray jsonParamArray;

    public JsonBase(String text)
    {
        try
        {
            json = new JSONObject(text);
/*
            if(json.getInt("codigo") != 0)
            {
                this.valid = false;
                this.errorMessage = json.getString("mensaje");
            }
            else
            {
                this.valid = true;
                this.errorMessage = "";
            }
*/
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error creando el JSON a partir del texto: " + text + " Error: " + e.getMessage());

            this.valid = false;
            this.errorMessage = "ERROR";
        }
    }

    public boolean isValid()
    {
        return this.valid;
    }

    public String error()
    {
        return this.errorMessage;
    }

    public void createjsonCommand() {
        jsonCommand = new JSONObject();
    }

    public void addJSONCommandToArray(JSONObject j) {
        if (jsonCommandArray == null) jsonCommandArray = new JSONArray();
        jsonCommandArray.put(j);
    }

    public void addJSONParamToArray(JSONObject j) {
        if (jsonParamArray == null) jsonParamArray = new JSONArray();
        jsonCommandArray.put(j);
    }

    public JSONObject getJsonCommand(int i) {
        JSONObject j = null;
        try {
            j = jsonCommandArray.getJSONObject(i);
        }catch (JSONException e) {

        }
        return j;
    }

    public JSONObject getJsonParam(int i) {
        JSONObject j = null;
        try {
            j = jsonParamArray.getJSONObject(i);
        }catch (JSONException e) {

        }
        return j;
    }

    public void createjsonParam() {
        jsonParam = new JSONObject();
    }

    public void addJsonCommand(String llave, String valor) {
        if (jsonCommand==null) createjsonCommand();
        try {
            jsonCommand.put(llave, valor);
        } catch (JSONException e) {

        }
    }

    public void addJsonParam(String llave, String valor) {
        if (jsonParam==null) createjsonParam();
        try {
            jsonParam.put(llave, valor);
        } catch (JSONException e) {

        }
    }




    public JSONObject getJsonCommand() { return jsonCommand;}

    public JSONObject getJsonParam() { return jsonParam;}
}