package com.freeway.cbc.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by macbookpro on 01/09/17.
 */

public class ConnectionDetector {

    private final static String TAG = "ConnectionDetector_LOG";

    private Context context;

    public ConnectionDetector(Context context)
    {
        this.context = context;
    }

    /**
     * Checking for all possible internet providers
     * **/
    public boolean isConnectingToInternet()
    {
        Log.v(TAG, "Chequear si hay disponible alguna conexion a Internet");

        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
            {
                for (int i = 0; i < info.length; i++)
                {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        Log.v(TAG, "Hay conexion a Internet disponible");
                        return true;
                    }
                }
            }
        }

        Log.v(TAG, "No hay conexion a Internet disponible ");
        return false;
    }
}
