package com.freeway.cbc.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by macbookpro on 04/09/17.
 */

public class Utils {

    private static final String currentDataPath = Environment.getDataDirectory().getAbsolutePath();


    public static String getDataDirectory() {
        return currentDataPath;
    }

    public static void writeToFile(String data,String toFile, Context context) {
        try {
            //toFile = currentDataPath + "/" + toFile;
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(toFile, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
            Log.e("COPIED", toFile + " - OK");
        }
        catch (IOException e) {
            Log.e("Exception", "File write FAILED: " + e.toString());
        }
    }

    public static String readFromFile(Context context, String fromFile) {

        String ret = "";
        //fromFile = currentDataPath + "/" + fromFile;


        try {
            InputStream inputStream = context.openFileInput(fromFile);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
                Log.e("READ", fromFile + " - OK - " + ret);
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static void deleteFile(String inputPath, String inputFile) {
        try {
            // delete the original file
            new File(inputPath + inputFile).delete();
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File (outputPath);
            if (!dir.exists())
            {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + inputFile);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        }  catch (FileNotFoundException fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }
        catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    public static String getNotNullString(String this_value) {
        if (this_value==null)
            return " ";
        else
            return this_value;
    }

    public static Double getNotNullDouble(Double this_value) {
        if (this_value==null)
            return 0.0d;
        else
            return this_value;
    }

}
