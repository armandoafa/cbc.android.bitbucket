package com.freeway.cbc.util;

/**
 * Created by macbookpro on 01/09/17.
 */

public interface OnServerResponseListener {

    public void onError(String error);
    public void onResponse(String response);
    public void onCancel();
}
