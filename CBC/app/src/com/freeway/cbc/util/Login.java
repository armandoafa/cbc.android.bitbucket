package com.freeway.cbc.util;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.freeway.cbc.R;
import com.freeway.cbc.domain.User;

/**
 * Created by macbookpro on 01/09/17.
 */

public class Login {
    private static final String TAG = Login.class.getName();

    public static void login(final Activity activity, String userName, final String password, final LoginListener listener)
    {

        double latitude = 0;
        double longitude = 0;
/*
        Toast.makeText(activity, "UUID: " + uuid, Toast.LENGTH_LONG).show();

        GPSTracker gps = new GPSTracker(activity);
        // check if GPS enabled
        if(gps.canGetLocation())
        {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            // \n is for new line
            Toast.makeText(activity, "Su ubicación es - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        }else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            //gps.showSettingsAlert();
            //return;
            //Toast.makeText(activity, "GPS no está habilitado", Toast.LENGTH_LONG).show();
        }
*/
        ServerUtilities.login(activity, userName, password, new OnServerResponseListener() {
            @Override
            public void onError(String error)
            {
                Log.e(TAG, "Error autenticando el ic_user en SF. Error: " + error);

                new AlertDialogManager().showAlertDialog(activity,
                        activity.getResources().getString(R.string.error),
                        activity.getResources().getString(R.string.error_logon_user),
                        ContextCompat.getDrawable(activity.getApplicationContext(), R.drawable.cbc_icon),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });

                listener.onError();
            }

            @Override
            public void onResponse(String response)
            {
                Log.d(TAG, "Autenticacion satisfactorio del user en SF!!. Response: " + response);

                JsonAuthenticateUserResponse json = new JsonAuthenticateUserResponse(response);
/*                if (!json.isValid())
                {
                    this.onError(json.error());
                    return;
                }
*/
                /*
                * TODO: Guardar los datos que vienen en el response del ic_user autenticado

                Log.d(TAG, "Se va a guardar los datos que vienen en el response del ic_user autenticado");

                SessionManager sessionManager = new SessionManager(activity);
                sessionManager.createLoginSession(json.userName, password, uuid);
                */

                User user = json.user;
                //Utils.setAuthenticatedUser(user);
                listener.onEnded(user);
            }

            @Override
            public void onCancel()
            {
                this.onError(activity.getResources().getString(R.string.canceled));
            }
        });
    }
}
