package com.freeway.cbc.util;

import android.util.Log;

import com.freeway.cbc.domain.User;

import org.json.JSONObject;

/**
 * Created by macbookpro on 01/09/17.
 */

public class JsonAuthenticateUserResponse extends JsonBase {

    final private String TAG = JsonAuthenticateUserResponse.class.getName();

    public User user;

    public JsonAuthenticateUserResponse(String text)
    {
        super(text);

        /**  FORMATO DE LA RESPUESTA
         {"codigo":"0","mensaje":"OK","datos":{"username":"master","uuid":"UUID-
         200000200","idcomercio":"200","idusuario":"103","comercio":"COMERCIO
         PRUEBAS","idrol":"2000","rol":"MASTER","email":"smoralesc81@gmail.com","telefono"
         :"5521058484","celular":"5521058484","status":1,"nombre":"master cellpanel
         mdmd"}}
         */
/*
        if (!super.isValid())
        {
            return;
        }
*/
        try
        {
            JSONObject jsonData = new JSONObject(text); //  super.json.getJSONObject("datos");

            this.user = new User();
            this.user.id = jsonData.getString("id");
            this.user.issued_at = jsonData.getString("issued_at");
            this.user.instance_url = jsonData.getString("instance_url");
            this.user.signature = jsonData.getString("signature");
            this.user.access_token = jsonData.getString("access_token");
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error creando el JSON a partir del texto: " + text + " Error: " + e.getMessage());

            super.valid = false;
        }
    }
}
