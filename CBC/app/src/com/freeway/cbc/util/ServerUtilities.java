package com.freeway.cbc.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.freeway.cbc.R;
import com.salesforce.androidsdk.rest.RestClient;

import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by macbookpro on 01/09/17.
 */

public class ServerUtilities {

    public static Context context;

    private static final String loginURL = "https://login.salesforce.com/services/oauth2/token";
    public static final String clientID = "3MVG99E3Ry5mh4zp0XHyOK865qKAsMduiXeQawd5BgiSgAeAVC83BJvlJmvnnuVbVmLHHsg.pPKJlkvUr0OZ1";
    public static final String clientSecret = "2638981368188753418";
    public static final String appURL = "https://desarrollo-cbc-dev.cs67.force.com/app/services/oauth2/token";

    private static final String TAG = ServerUtilities.class.getName();
    private static final int MAX_ATTEMPTS = 5;

    //Refreshing data
    //0: Account, 1:Encuesta, 2:Pregunta, 3:Respuesta, 4: Pregunta-Respuesta, 5: Publico-Objetivo, 6: Pago
    public static final String sqlAccount = "SELECT id, SAP_code__c, Description, Name, Route__c, ShippingAddress, BillingAddress, Channel__c, Subchannel__c, Labeled__c, Account_type__c, Share__c, Refrigeration_model__c FROM Account";
    public static final String sqlInquiry = "SELECT id, Name, Survey_type__C, Start_date__c, End_date__c, Status__c, Accountables__c from Inquiry__c";
    public static final String sqlQuestion = "SELECT id, Name, RecordTypeId, Description__c, Survey__c, Type__c, Active__c, Rating__c, Required__c, Unique__c, Section_KPI__c, Section_rating__c from Question__c";
    public static final String sqlAnswer = "SELECT id, Name, Question__c, RecordTypeId, Yes_No_value__c, Date_value__c, Text_value__c, Number_value__c, Select_value__c, Multiselect_values__c from Answer__c";
    public static final String sqlQuestionAnswer = "SELECT id, Cicle__c, Account__c, Name, CreatedById, Survey__c, Offline_Id__c, Previous_month_color__c, Previous_month_signal__c, Evaluated_month_color__c, Evaluated_month_signal__c, Question__c, Response__c, RecordTypeId,LastModifiedById from Question_Answer__c";
    public static final String sqlTargetAudiences = "SELECT id, Account__c, Name, CreatedById, Survey__c, Date__c, Offline_id__c, Rejection_reason__c, Account_name__c, Record__c, Phone__c, Type__c, Tipo_de_cliente__c, Touch_point__c, LastModifiedById from Target_Audiences__c";
    public static final String sqlPaumentPending = "SELECT id, Client__c, CreatedById, Segmentation_code__c, Invoice_age__c, Expiration_days__c, Invoice_date__c, Expiration_date__c, Name, OwnerId, Invoice_amount__c, Segmentation_description__c, Expiration_signal__c, Is_expired__c, LastModifiedById from Payment_Pending__c";
    public static final String sqlRoute = "SELECT id, Client__c, CreatedById, Name from Route__c";

    private static String serverUrl;

    public static RestClient restClient;

    public static JSONObject jsonCustomer;
    public static JSONObject jsonRoute;


    public static void login(Activity activity, String username, String password, OnServerResponseListener listener)
    {

        serverUrl = appURL;
        serverUrl = serverUrl + "?username=" + username;
        serverUrl = serverUrl + "&grant_type=password";
        serverUrl = serverUrl + "&client_id=" + clientID;
        serverUrl = serverUrl + "&client_secret=" + clientSecret;
        serverUrl = serverUrl + "&password=" + password;
        try
        {
            Map<String, String> params = new HashMap<String, String>();
            post(activity, serverUrl, params, activity.getString(R.string.server_utilities_wait_text_login), listener);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error accediendo al servidor desde el método 'login': " + e.getMessage());

            listener.onError(activity.getString(R.string.error_unexpected_error_on_internet_access));
        }
    }


    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static void post(final Context context, String endpoint, Map<String, String> params, final String wait_text,
                             final OnServerResponseListener listener)
            throws IOException
    {
        post (context, endpoint, params, wait_text, true, listener);
    }

    private static void post(final Context context, String endpoint, Map<String, String> params, final String wait_text, final boolean showWaitText,
                             final OnServerResponseListener listener)
            throws IOException
    {
        // Check if Internet present
        ConnectionDetector connectionDetector = new ConnectionDetector(context);
        if (!connectionDetector.isConnectingToInternet())
        {
            // Internet Connection is not present
            listener.onError(context.getString(R.string.global_error_no_internet_connection_available));
            // stop executing code by return
            return;
        }

        final URL url;
        try
        {
            url = new URL(endpoint);
        }
        catch (MalformedURLException e)
        {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }

        Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        JSONObject json = new JSONObject();
        try {

            while (iterator.hasNext())
            {
                Map.Entry<String, String> param = iterator.next();
                json.put(param.getKey(), param.getValue());
            }
        }
        catch(Exception e)
        {
            throw new IllegalArgumentException("Error creando el Json, error: " + e.getMessage());
        }

        final String body = json.toString();
        Log.d(TAG, "Posting '" + body + "' to " + url);

        try
        {
            final ProgressDialog progressDialog = new ProgressDialog(context);

            final AsyncTask<String, String, String> async = new AsyncTask<String, String, String> (){

                @Override
                protected void onPreExecute()
                {
                    if (showWaitText)
                    {
                        progressDialog.setMessage(wait_text/*R.string.server_utilities_wait_text*/);
                    }
                }

                @Override
                protected String doInBackground(String... params)
                {
                    for(int i = 0; i < MAX_ATTEMPTS; i++)
                    {
                        Log.v(TAG, "Intento #" + i + " para acceder al servidor");

                        byte[] bytes = body.getBytes();
                        HttpURLConnection conn = null;
                        try
                        {
                            Log.d(TAG, "> " + url);


                            conn = (HttpsURLConnection) url.openConnection();
                            conn.setUseCaches(false);
                            conn.setDoOutput(true);
                            conn.setFixedLengthStreamingMode(bytes.length);
                            conn.setRequestMethod("POST");
                            String cc = conn.getRequestMethod();
                            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                            conn.setRequestProperty( "charset", "utf-8");
                            conn.setUseCaches( false );
                            //conn.setRequestProperty("Content-Type", "application/json");

                            // post the request
                            OutputStream out = conn.getOutputStream();
                            out.write(bytes);
                            //out.write(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                            out.close();

                            // handle the response
                            String statusMsg =  conn.getResponseMessage();
                            int status = conn.getResponseCode();
                            if (status != 200 && status != 201)
                            {
                                //throw new IOException("Post failed with error code " + status);
                            }
                            if(statusMsg.length()>0)
                            {
                                Log.e("Resp",statusMsg);
                            }

                            InputStream stream = conn.getInputStream();
                            StringBuffer sb = new StringBuffer();
                            int chr;
                            while ((chr = stream.read()) != -1)
                            {
                                sb.append((char) chr);
                            }
                            String response = new String(sb.toString().getBytes()/*,"UTF-8"*/);

                            Log.v(TAG, "La respuesta del servidor es: " + response);

                            return response;
                        }
                        catch (Exception e)
                        {
                            // Here we are simplifying and retrying on any error; in a real
                            // application, it should retry only on unrecoverable errors
                            // (like HTTP error code 503).
                            String x = e.getMessage();
                            Log.e(TAG, "Falló el registro en el intento: " + i + " : " + e.getMessage(), e);
                        }
                        finally
                        {
                            if (conn != null)
                            {
                                conn.disconnect();
                            }

                            Log.v(TAG, "Finalizaron los accesos al servidor");
                        }
                    }

                    return "";
                }

                @Override
                protected void onPostExecute(String result)
                {
                    if (showWaitText)
                    {
                        progressDialog.dismiss();
                    }

                    //if (async.isCancelled() == true)
                    //{
                    //	listener.onCancel();
                    //}
                    //else
                    //{
                    if (result.length() != 0)
                    {
                        listener.onResponse(result);
                    }
                    else
                    {
                        listener.onError("Error en la respuesta obtenida del servidor");
                    }
                    //}

                    //async = null;

                    Log.d(TAG, "Asignada la respuesta a la interface 'OnServerResponse', valor: " + result);
                }

            };

            async.execute("", "", "");
        }
        catch (Exception e)
        {
            listener.onError(context.getString(R.string.error_unexpected_error_on_internet_access));
            Log.e(TAG, "Error inesperado en el acceso al servidor error: " + e.getMessage(), e);
        }
    }

}
