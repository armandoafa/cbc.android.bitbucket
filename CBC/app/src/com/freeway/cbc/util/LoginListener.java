package com.freeway.cbc.util;

import com.freeway.cbc.domain.User;

/**
 * Created by macbookpro on 01/09/17.
 */

public interface LoginListener {

    void onEnded(User user);
    void onError();
    void onCancel();
}
