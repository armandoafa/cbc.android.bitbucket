package com.freeway.cbc.controller;

import android.util.Log;
import android.widget.Toast;

import com.freeway.cbc.CustomerDashboardActivity;
import com.freeway.cbc.util.ServerUtilities;
import com.freeway.cbc.util.Utils;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ApiVersionStrings;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.google.android.gms.internal.zzagy.runOnUiThread;

/**
 * Created by macbookpro on 05/09/17.
 */

public class AccountController {

    private RestClient client;
    private JSONArray records;

    public AccountController(RestClient this_client) {
        client = this_client;
    }

    public void setRecords(JSONArray ja) {
        records = ja;
    }

    public JSONArray getRecords() {
        return records;
    }

    public JSONArray getRecordFromLocal() {
        String leido = Utils.readFromFile(ServerUtilities.context,"Account");
        JSONObject json = new JSONObject();
        records = new JSONArray();
        try {
            json = new JSONObject(leido);
            records = json.getJSONArray("records");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return records;
    }


}
