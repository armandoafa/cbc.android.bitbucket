package com.freeway.cbc.controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.freeway.cbc.CustomerDashboardActivity;
import com.freeway.cbc.CustomerMapActivity;
import com.freeway.cbc.MainActivity;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.smartsync.manager.SyncManager;
import com.salesforce.androidsdk.smartsync.util.SyncState;

/**
 * Created by macbookpro on 02/09/17.
 */

public class MainController {

    public Context context;

    ClientManager clientManager;
    SyncManager syncManager;
    SyncManager.SyncUpdateCallback syncUpdateCallBack;

    public static String currentRoute;

    public static Intent intent_dashBoard;
    public static Intent intent_mapBoard;


    RestClient restClient;

    String accountType;
    ClientManager.LoginOptions loginOptions;

    public MainController() {

    }

    public void syncDown() {
        syncUpdateCallBack = new SyncManager.SyncUpdateCallback() {
            @Override
            public void onUpdate(SyncState sync) {

            }
        };
    }

    public static void activeIntentDashBoard(Context context) {
        intent_dashBoard = new Intent(context,
                CustomerDashboardActivity.class);
    }

    public static Intent getIntentDashBoard(Context context) {
        if (intent_dashBoard == null) {
            intent_dashBoard = new Intent(context,
                    CustomerDashboardActivity.class);
        }
        return intent_dashBoard;
    }

    public static Intent getIntentMapBoard(Context context) {
        if (intent_mapBoard == null) {
            intent_mapBoard = new Intent(context,
                    CustomerMapActivity.class);
        }
        return intent_mapBoard;
    }


}


